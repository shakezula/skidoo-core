Skidoo integration/staging tree
=====================================

http://skidoo.info
ALGORITHM: SHA256D
MERGE MINED AFTER BLOCK # 55000

What is Skidoo?
----------------

Skidoo is an experimental new digital currency that enables instant payments to
anyone, anywhere in the world. Skidoo uses peer-to-peer technology to operate
with no central authority: managing transactions and issuing money are carried
out collectively by the network. Skidoo Core is the name of open source
software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the CHAO Kore software, see http://skidoo.info.

License
-------

Skidoo Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see http://opensource.org/licenses/MIT.


